/* ToDo
	use only response matrix, without length array
	add algorithm parameters to gui
*/

  // In the following example, markers appear when the user clicks on the map.
  // The markers are stored in an array.
  // The user can then click an option to hide, show or delete the markers.
var map;
var markers = [];
var directionsService;
var directionsDisplay;
var directionsDisplayArray = [];
var progressValue;
var totalProgress;
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
var labelIndex = 0;

var markersDirty = false;
var lengthArray = [];
var responseMatrix = [];

    
var selectedMode;
  
  function initMap() {
	var startingPosition = {lat: 52, lng: 19};
	directionsService = new google.maps.DirectionsService;
	directionsDisplay = new google.maps.DirectionsRenderer;
      
	map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 7,
	  center: startingPosition,
	  mapTypeId: google.maps.MapTypeId.TERRAIN
	});
      
	directionsDisplay.setMap(map);

	// This event listener will call addMarker() when the map is clicked.
	map.addListener('click', function(event) {
	  addMarker(event.latLng);
	});
	
  }

  // Adds a marker to the map and push to the array.
  function addMarker(location) {
	var marker = new google.maps.Marker({
	  position: location,
	  label: "",
	  map: map
	});
	markers.push(marker);
	markersDirty = true;
  }

  // Sets the map on all markers in the array.
  function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
	  markers[i].setMap(map);
	}
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {
	setMapOnAll(null);
  }

function clearDirections(fromMapOnly){
	var fmo = fromMapOnly || false;

    for(var i = 0; i < directionsDisplayArray.length; i++){
        directionsDisplayArray[i].setMap(null);
        if(!fromMapOnly) {
	        directionsDisplayArray[i] = null;
	    }
    }
}

  // Shows any markers currently in the array.
  function showMarkers() {
	setMapOnAll(map);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkers() {
	clearMarkers();
	markers = [];
    clearDirections();
    directionsDisplayArray = [];
    markersDirty = true;
  }
  
  function showGuide(){
      console.error("Guide pressed");
  }
  
  function calculatePath(){
      labelIndex = 0;

      currentlySelectedMode = document.getElementById("mode").value;

      if(currentlySelectedMode !== selectedMode) {
      	markersDirty = true;
      	selectedMode = currentlySelectedMode;
      }

      if(markersDirty) {
      	console.log('GMaps data dirty, need to reload');
	    clearDirections(false);
	    directionsDisplayArray = [];

	    progressValue = 0;
      	totalProgress = markers.length*markers.length;

		calculateAndDisplayRoute(selectedMode, directionsService, directionsDisplay);
	  } else {
	  	console.log('Cached GMaps data up-to-date, reusing them');
	  	clearDirections(true);

    	progressValue = 1;
      	totalProgress = 1;

	  	onPathDataLoaded(lengthArray, responseMatrix, directionsDisplay);
	  }
  }

function move() {
    var elem = document.getElementById("bar"); 
    var width = Math.floor(100*progressValue/totalProgress);
    if(width === NaN){
        width = 0;
    }
    elem.style.width = width + '%'; 
    document.getElementById("label").innerHTML = width * 1 + '%';
}
  
  function calculateAndDisplayRoute(selectedMode, directionsService, directionsDisplay) {
	  var cities = markers.length

	  lengthArray = new Array(cities);
	  responseMatrix = new Array(cities);
	  for(var i = 0; i < cities; i++){
		  lengthArray[i] = new Array(cities);
		  responseMatrix[i] = new Array(cities)
	  }

	  var latch = new CountdownLatch(cities*cities)

	  setTimeout(function() {getTimeDistance(directionsService, directionsDisplay, 0, cities-1, 0, cities-1, lengthArray, latch, responseMatrix)}, 0);
	  console.log("awaiting all path length info")
	  
	  latch.await(function(){onPathDataLoaded(lengthArray, responseMatrix, directionsDisplay)})
	}

	function getTimeDistance(directionsService, directionsDisplay, i, maxi, j, maxj, lengthArray, latch, responseMatrix){
		console.log('getTimeDistance for %d %d', i, j)
		directionsService.route({
				origin: markers[i].position,
				destination: markers[j].position,
				travelMode: selectedMode
			}, 
			function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
			   // schedule next
			   if(i != maxi || j != maxj) {
			   		var nextI = 0;
				   var nextJ = 0;
				   if(j == maxj) {
				   		nextI = i+1;
				   		nextJ = 0;
				   } else {
				   		nextI = i;
				   		nextJ = j+1;
				   }

				   //schedule next
				   setTimeout(function() {getTimeDistance(directionsService, directionsDisplay, nextI, maxi, nextJ, maxj, lengthArray, latch, responseMatrix)}, 400);
			   }
			   

			  responseMatrix[i][j] = response
			  lengthArray[i][j] = response.routes[0].legs[0].duration.value;
			  latch.countDown()
			  console.log('OK response received with value ' + lengthArray[i][j])
			  progressValue += 1;
			  move();
		} else {
			console.log('Directions request for (%d, %d) failed due to ' + status, i, j)
			setTimeout(function() {getTimeDistance(directionsService, directionsDisplay, i, maxi, j, maxj, lengthArray, latch, responseMatrix)}, 1000);
		}
		});
	}
	
var time;
var iteration;
var solverTime;
	function onPathDataLoaded(lengthArray, responseMatrix, directionsDisplay) {
		console.log("all data loaded")
        
		markersDirty = false;
	  	progressValue = totalProgress;
      	move();

        queryTspServer(lengthArray, getParams(), function(r) {
			var dirs = buildRouteArray(r.vector, responseMatrix)
            displayNumbersOnMarkers(r.vector)
            time = r.cost;
            iteration = r.iteration;
            solverTime = r.computation_time;
			for(var i = 0; i < dirs.length; i++) {
				var newDirectionsDisplay = new google.maps.DirectionsRenderer(
					{
					suppressMarkers: true,
                    preserveViewport: true
					})	
				newDirectionsDisplay.setMap(map)
				newDirectionsDisplay.setDirections(dirs[i])
                directionsDisplayArray.push(newDirectionsDisplay);
			}
            window.location.href = window.location.href.replace("#", "#results");
            
//            window.alert("Total route is " + parseSecondsToTime(time) + "." +
//                        "Solver found best solution at iteration number " + iteration);
            document.getElementById("resultBox").innerHTML = "Total route is " + parseSecondsToTime(time) + ". " +
                        "Solver found best solution at iteration number " + iteration 
            + ". Solver running time: " + solverTime;
		})
        window.location.href = window.location.href.replace("#loading", "#");
	}
    function parseSecondsToTime(time) {
        seconds = time % 60;
        time = Math.floor(time / 60);
        minutes = time % 60;
        time = Math.floor(time / 60);
        return time + " hours " + minutes + " minutes " + seconds + " seconds";
    }

    function displayNumbersOnMarkers(cities){
        console.log("Setting up numbers")
        for(var i = 0; i < cities.length; i++){
            markers[cities[i]].setLabel(labels[labelIndex++ % labels.length]);
        }
    }

	function buildRouteArray(cityList, responseMatrix) {
		var responsesOnRoute = []
		var intCityList = cityList.map(function(s){return parseInt(s)})
		for(var i = 0; i < intCityList.length - 1; i++) {
			var segmentStart = intCityList[i]
			var segmentEnd = intCityList[i+1]
			responsesOnRoute.push(responseMatrix[segmentStart][segmentEnd])
		}
        responsesOnRoute.push(responseMatrix[intCityList[intCityList.length-1]][intCityList[0]]);

		return responsesOnRoute
	}


    function getParams(){
    	return {
	        max_it:  document.getElementById("maxIter").value || 100,
	        c_heur: document.getElementById("c_heur").value || 5.0,
	        c_hist: document.getElementById("c_hist").value || 0.5,
	        decoy_factor: document.getElementById("decay_factor").value || 0.8,
	        num_ants: document.getElementById("num_ants").value || markers.length,
        }
    }
