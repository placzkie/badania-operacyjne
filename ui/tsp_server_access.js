
var tspServerAddress = 'http://shnek.tk:8082'

function queryTspServer(distanceMatrix, params, responseCb) {
	var address = requestUrl(distanceMatrix, params)
	console.log(address)
	sendReqest(address, responseCb)
}

function sendReqest(address, responseCb) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
    	if(xmlHttp.readyState == 4) {
    		console.log("%d\n%s", xmlHttp.status, xmlHttp.responseText)
			var response = JSON.parse(xmlHttp.responseText)	
			responseCb(response)
    	} else {
    		console.log("reponse readyState changed to %d", xmlHttp.readyState)
    	}
    }
    xmlHttp.open("POST", address, true);
    xmlHttp.send(null);
}

function requestUrl(distanceMatrix, params) {
	var clist = cityList(distanceMatrix)
	var dmap = distanceMap(distanceMatrix)
	var encodedParams = encodeParams(params)
	return tspServerAddress + '?cities=' + clist + '&distances=' + dmap + encodedParams
}

function encodeParams(params) {
	var paramString = ""
	for (var param in params) {
	    if (params.hasOwnProperty(param)) {
	        paramString += "&" + param + "=" + params[param]
	    }
	}
	return paramString
}

function cityList(distanceMatrix) {
	var list = '['
	var i = 0
	if(distanceMatrix.length >= 1) {
		list += '"' + (i++).toString() + '"'
		while(i < distanceMatrix.length) {
			list += ',"' + (i++).toString() + '"'
		}
	}
	list += ']'
	return list
}

function distanceMap(distanceMatrix) {
	var list = '{'
	if(distanceMatrix.length >= 2) {
		list += mapEntry(distanceMatrix, 0, 1)
		
		for(var j = 2; j < distanceMatrix.length; j++) {
			list += ',' + mapEntry(distanceMatrix, 0, j)
		}

		for(var i = 1; i < distanceMatrix.length; i++) {
			for(var j = 0; j < distanceMatrix.length; j++) {
				if(i != j) {
					list += ',' + mapEntry(distanceMatrix, i, j)
				}
			}
		}
	}
	list += '}'
	return list
}

function mapEntry(distanceMatrix, i, j) {
	return '("' + i.toString() + '","' + j.toString() + '"):' + distanceMatrix[i][j].toString()
}

/* 127.0.0.1:5000?cities=['a','b','c']&distances={('a', 'b'):15,('b','a'):10} */
