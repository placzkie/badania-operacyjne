import math
import AntSystem
import utils

def read_data(filename):
    coordinates = []
    distances = []
    save_coordinates = False
    with open(filename, 'r') as f:
        for line in f:
            if line.startswith("DIMENSION"):
                colon_idx = line.find(":")
                dim = int(line[colon_idx + 1:])
                distances = [[0 for x in range(dim)]for x in range(dim)]
            if save_coordinates == True:
                if not line.startswith("EOF"):
                    row_content = line.split()
                    coordinates.append((int(row_content[1]), int(row_content[2])))
            if line.startswith("NODE_COORD_SECTION"):
                save_coordinates = True
    calculate_distances(coordinates, distances)
    return prepare_data_for_solver(distances)

def calculate_distances(coordinates, distances):
    for idx1, p1 in enumerate(coordinates):
        for idx2, p2 in enumerate(coordinates):
            if not p1 == p2:
                distances[idx1][idx2] = calculate_distance(p1, p2)

def calculate_distance(p1, p2):
    return int(round(math.sqrt(((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2))))

def prepare_data_for_solver(distance_matrix):
    cities = []
    distances = {}
    for row_idx, row in enumerate(distance_matrix):
        cities.append(row_idx)
        for col_idx, val in enumerate(row):
            if row_idx != col_idx:
                distances[(row_idx, col_idx)] = val
    return cities, distances

def main():
    cities, distances = read_data("../tsplib_data/eil51.tsp")

    default_max_it, default_c_heur, default_c_hist, default_decay_factor = utils.load_default_parameters()

    algorithm_parameters = {
        'max_it': default_max_it,
        'c_heur': default_c_heur,
        'c_hist': default_c_hist,
        'decay_factor': default_decay_factor,

        'cities': cities,
        'distances': distances,
        'num_ants': len(cities),
    }

    print(AntSystem.search(**algorithm_parameters))

if __name__ == '__main__':
    main()