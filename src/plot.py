import os

import matplotlib.pyplot as plt
import numpy as np

import AntSystem
import utils
from TsplibReaderATSP import read_data


def main():
    algorithm_parameters, cities, distances = load_parameters('../tsplib_data/ftv55.atsp')
    number_of_retries = 10
    algorithm_parameters['max_it'] = 10
    test_parameter(algorithm_parameters, 'decay_factor', np.linspace(0, 1, 10), number_of_retries)


def load_parameters(problem):
    cities, distances = read_data(problem)
    default_max_it, default_c_heur, default_c_hist, default_decay_factor = utils.load_default_parameters()
    algorithm_parameters = {
        'max_it': default_max_it,
        'c_heur': default_c_heur,
        'c_hist': default_c_hist,
        'decay_factor': default_decay_factor,

        'cities': cities,
        'distances': distances,
        'num_ants': len(cities),
    }
    return algorithm_parameters, cities, distances


def test_parameter(algorithm_parameters, parameter_name, parameter_values, number_of_retries):
    plots_directory = '../plots/'
    if not os.path.exists(plots_directory):
        os.makedirs(plots_directory)

    iterations_number = algorithm_parameters['max_it']
    for parameter_value in parameter_values:
        algorithm_parameters[parameter_name] = parameter_value

        sum_history = np.zeros(iterations_number)
        for retry in xrange(number_of_retries):
            history, _ = AntSystem.solve(**algorithm_parameters)
            sum_history += history[:iterations_number]

        mean_history = sum_history / number_of_retries
        # to avoid extremely steep beginning of plot
        mean_history = mean_history[1:]

        label = '{} = {:.2f}'.format(parameter_name, parameter_value)
        plt.plot(range(1, len(mean_history) + 1), mean_history, label=label)

    lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    ax = plt.axes()
    ax.set_xlabel('iteration number')
    ax.set_ylabel('best cost so far')

    plot_name = '{}.png'.format(parameter_name)
    plot_path = os.path.join(plots_directory, plot_name)
    plt.savefig(plot_path, bbox_extra_artists=(lgd,), bbox_inches='tight')


if __name__ == '__main__':
    main()
