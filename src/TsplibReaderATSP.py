import AntSystem
import utils

row = 0
col = 0
dim = 0
def read_data(filename):
    global dim
    distances = []
    save_distances = False
    with open(filename, 'r') as f:
        for line in f:
            if line.startswith("DIMENSION"):
                colon_idx = line.find(":")
                dim = int(line[colon_idx + 1:])
                distances = [[0 for x in range(dim)]for x in range(dim)]
            if save_distances == True:
                if not line.startswith("EOF"):
                    for w in line.split():
                        add_distance(int(w), distances)
            if line.startswith("EDGE_WEIGHT_SECTION"):
                save_distances = True
    return prepare_data_for_solver(distances)

def add_distance(distance, distances):
    global col
    global row
    distances[row][col] = distance
    update_indices()

def update_indices():
    global col
    global row
    if col == dim - 1:
        col = 0
        row += 1
    else:
        col += 1

def prepare_data_for_solver(distance_matrix):
    cities = []
    distances = {}
    for row_idx, row in enumerate(distance_matrix):
        cities.append(row_idx)
        for col_idx, val in enumerate(row):
            if row_idx != col_idx:
                distances[(row_idx, col_idx)] = val
    return cities, distances

def main():
    cities, distances = read_data("../tsplib_data/ftv55.atsp")

    default_max_it, default_c_heur, default_c_hist, default_decay_factor = utils.load_default_parameters()

    algorithm_parameters = {
        'max_it': default_max_it,
        'c_heur': default_c_heur,
        'c_hist': default_c_hist,
        'decay_factor': default_decay_factor,

        'cities': cities,
        'distances': distances,
        'num_ants': len(cities),
    }

    print(AntSystem.search(**algorithm_parameters))

if __name__ == '__main__':
    main()