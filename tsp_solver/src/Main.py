import AntSystem
import utils


def main():
    cities = ["a", "b", "c"]
    distances = {("a","b"): 15, ("b", "a"): 10, ("a", "c"): 3, ("c", "a"): 5, ("b", "c"): 7, ("c", "b"): 5}

    default_max_it, default_c_heur, default_c_hist, default_decay_factor = utils.load_default_parameters()

    algorithm_parameters = {
        'max_it': default_max_it,
        'c_heur': default_c_heur,
        'c_hist': default_c_hist,
        'decay_factor': default_decay_factor,

        'cities': cities,
        'distances': distances,
        'num_ants': len(cities),
    }

    print(AntSystem.search(**algorithm_parameters))


if __name__ == '__main__':
    main()
