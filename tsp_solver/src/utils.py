from ConfigParser import ConfigParser


def load_default_parameters():
    main_config = ConfigParser()
    main_config.read('../config/algorithm_defaults.ini')

    default_max_it = main_config.getint('parameters', 'max_it')
    default_c_heur = main_config.getfloat('parameters', 'c_heur')
    default_c_hist = main_config.getfloat('parameters', 'c_hist')
    default_decay_factor = main_config.getfloat('parameters', 'decay_factor')

    return default_max_it, default_c_heur, default_c_hist, default_decay_factor
