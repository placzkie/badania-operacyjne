from __future__ import division

from random import randint, uniform


def compute_distance(c1, c2, distances):
    return distances[(c1, c2)]


def random_permutation(cities):
    permutation = range(len(cities))
    for i in permutation:
        r = randint(0, len(permutation) - i - 1) + i
        permutation[r], permutation[i] = permutation[i], permutation[r]
    return permutation


def cost(permutation, cities, distances):
    distance = 0
    for idx, c1 in enumerate(permutation):
        c2 = permutation[0] if (idx == len(permutation) - 1) else permutation[idx + 1]
        distance += compute_distance(cities[c1], cities[c2], distances)
    return distance


def initialise_pheromone_matrix(num_cities, naive_score):
    v = num_cities / naive_score
    return [[v] * num_cities] * num_cities


def stepwise_const(cities, phero, c_heur, c_hist, ant_num, distances):
    perm = [ant_num]
    while len(perm) != len(cities):
        choices = calculate_choices(cities, perm[-1], perm, phero, c_heur, c_hist, distances)
        next_city = select_next_city(choices)
        perm.append(next_city)
    return perm


def select_next_city(choices):
    _sum = 0.0
    for choice in choices:
        _sum += choice['prob']
    if _sum == 0.0:
        return choices[randint(0, choices.size)]['city']
    v = uniform(0.0, 1.0)
    for idx, choice in enumerate(choices):
        v -= (choice['prob'] / _sum)
        if v <= 0.0:
            return choice['city']
    return choices[-1]['city']


def calculate_choices(cities, last_city, exclude, pheromone, c_heur, c_hist, distances):
    choices = []
    for idx, city in enumerate(cities):
        if idx in exclude:
            continue
        history = pheromone[last_city][idx] ** c_hist
        distance = compute_distance(cities[last_city], city, distances)
        heuristic = (1.0 / distance) ** c_heur
        prob = {
            'city': idx,
            'history': history,
            'distance': distance,
            'heuristic': heuristic,
            'prob': history * heuristic
        }
        choices.append(prob)
    return choices


def decay_pheromone(pheromone, decay_factor):
    for array in pheromone:
        for idx, p in enumerate(array):
            array[idx] = (1.0 - decay_factor) * p


def update_pheromone(pheromone, solutions):
    for other in solutions:
        for idx, x in enumerate(other['vector']):
            y = other['vector'][0] if (idx == len(other['vector']) - 1) else other['vector'][idx + 1]
            pheromone[x][y] += (1.0 / other['cost'])
            # pheromone[y][x] += (1.0 / other['cost'])


def solve(cities, max_it, num_ants, decay_factor, c_heur, c_hist, distances):
    best_vector = random_permutation(cities)
    best = {
        'vector': best_vector,
        'cost': cost(best_vector, cities, distances),
        'iteration': 0
    }
    best_solution_history = [best['cost']]
    pheromone = initialise_pheromone_matrix(len(cities), best['cost'])
    for i in range(0, max_it):
        solutions = []
        for j in range(0, num_ants):
            candidate_vector = stepwise_const(cities, pheromone, c_heur, c_hist, j % len(cities), distances)
            candidate = {
                'vector': candidate_vector,
                'cost': cost(candidate_vector, cities, distances),
                'iteration': i + 1
            }
            if candidate['cost'] < best['cost']:
                # print("Better found")
                best = candidate
            solutions.append(candidate)
        decay_pheromone(pheromone, decay_factor)
        update_pheromone(pheromone, solutions)
        best_solution_history.append(best['cost'])
        print("Iteration {0}, best={1}, vector={2}".format(i + 1, best['cost'], best['vector']))
    return best_solution_history, best


def search(cities, max_it, num_ants, decay_factor, c_heur, c_hist, distances):
    return solve(cities, max_it, num_ants, decay_factor, c_heur, c_hist, distances)[1]
