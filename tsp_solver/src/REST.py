import argparse
from ast import literal_eval

from flask import Flask
from flask.ext.cors import CORS
from flask.ext.restful import reqparse
from flask_restful import Resource, Api

import AntSystem
from utils import load_default_parameters

import time

app = Flask(__name__)
api = Api(app)
CORS(app, resources={r"/": {"origins": "*"}})


def parse_request():
    default_max_it, default_c_heur, default_c_hist, default_decay_factor = load_default_parameters()

    parser = reqparse.RequestParser()
    parser.add_argument('max_it', default=default_max_it, type=int, required=False)
    parser.add_argument('c_heur', default=default_c_heur, type=float, required=False)
    parser.add_argument('c_hist', default=default_c_hist, type=float, required=False)
    parser.add_argument('decay_factor', default=default_decay_factor, type=float, required=False)

    parser.add_argument('num_ants', type=int, required=False, help='Number of ants (default: len(cities)')
    parser.add_argument('cities', default=0, type=str, required=True, help='String encoded cities')
    parser.add_argument('distances', default=0, type=str, required=True,
                        help='String encoded distances between each pair of cities')
    args = parser.parse_args()

    args.cities = literal_eval(args.cities)
    args.distances = literal_eval(args.distances)

    if not args.num_ants:
        args.num_ants = len(args.cities)

    return args


class Path(Resource):
    @staticmethod
    def post():
        algorithm_parameters = parse_request()

        start_time = time.time()
        result = AntSystem.search(**algorithm_parameters)
        end_time = time.time()

        result['computation_time'] = end_time - start_time

        return result


api.add_resource(Path, '/')


def parse_args(default_host, default_port):
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, nargs='?', default=default_host)
    parser.add_argument('--port', type=int, nargs='?', default=default_port)
    args = parser.parse_args()
    return args


def main():
    args = parse_args(default_host='0.0.0.0', default_port=8082)
    app.run(debug=True, host=args.host, port=args.port, threaded=True)


if __name__ == '__main__':
    main()
